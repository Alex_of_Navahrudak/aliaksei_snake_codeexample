using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField] private Material pillsMaterial;
    private Animator animator;
    private int animationId;

    void Start()
    {
        animationId = Animator.StringToHash("Swallow");
        animator = GetComponent<Animator>();
    }

    public void MakeItSmaller()
    {
        animator.SetBool(animationId, true);
    }

    public void TurnItOff()
    {
        gameObject.SetActive(false);
    }
}
