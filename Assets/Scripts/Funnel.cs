using UnityEngine;

//This script calls the Food swallowing (it is on the throat-funnle object in the front of the Snake)
public class Funnel : MonoBehaviour
{
    private SnakeController snake;
    private string foodTag = "Food";
    private Color targetColor;

    private void Start()
    {
        snake = transform.parent.GetComponent<SnakeController>();
    }

    public void SetTargetColor(Color col) => targetColor = col;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == foodTag)
        {
            Material foodMat = other.GetComponent<Renderer>().material;

            if (targetColor == foodMat.color)
            {
                other.gameObject.GetComponent<Food>().MakeItSmaller();
                StartCoroutine(snake.Swallow());
            }
        }
    }
}
