// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""SnakeHead"",
            ""id"": ""4ef6657a-1c94-4a44-aec4-d494a4fab890"",
            ""actions"": [
                {
                    ""name"": ""Moving"",
                    ""type"": ""Button"",
                    ""id"": ""82835e42-0ce7-41d6-8055-f9a2a654b9c4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Touch Position"",
                    ""type"": ""PassThrough"",
                    ""id"": ""794f744f-8869-4acf-aabb-a4f8409d99c3"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""aec3f8ee-5c23-4eb3-bec7-a23fb287b077"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moving"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""85be2d62-cb0c-4b4c-b272-d448ceaaee51"",
                    ""path"": ""<Touchscreen>/primaryTouch/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Touch Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // SnakeHead
        m_SnakeHead = asset.FindActionMap("SnakeHead", throwIfNotFound: true);
        m_SnakeHead_Moving = m_SnakeHead.FindAction("Moving", throwIfNotFound: true);
        m_SnakeHead_TouchPosition = m_SnakeHead.FindAction("Touch Position", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // SnakeHead
    private readonly InputActionMap m_SnakeHead;
    private ISnakeHeadActions m_SnakeHeadActionsCallbackInterface;
    private readonly InputAction m_SnakeHead_Moving;
    private readonly InputAction m_SnakeHead_TouchPosition;
    public struct SnakeHeadActions
    {
        private @Controls m_Wrapper;
        public SnakeHeadActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Moving => m_Wrapper.m_SnakeHead_Moving;
        public InputAction @TouchPosition => m_Wrapper.m_SnakeHead_TouchPosition;
        public InputActionMap Get() { return m_Wrapper.m_SnakeHead; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SnakeHeadActions set) { return set.Get(); }
        public void SetCallbacks(ISnakeHeadActions instance)
        {
            if (m_Wrapper.m_SnakeHeadActionsCallbackInterface != null)
            {
                @Moving.started -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnMoving;
                @Moving.performed -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnMoving;
                @Moving.canceled -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnMoving;
                @TouchPosition.started -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnTouchPosition;
                @TouchPosition.performed -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnTouchPosition;
                @TouchPosition.canceled -= m_Wrapper.m_SnakeHeadActionsCallbackInterface.OnTouchPosition;
            }
            m_Wrapper.m_SnakeHeadActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Moving.started += instance.OnMoving;
                @Moving.performed += instance.OnMoving;
                @Moving.canceled += instance.OnMoving;
                @TouchPosition.started += instance.OnTouchPosition;
                @TouchPosition.performed += instance.OnTouchPosition;
                @TouchPosition.canceled += instance.OnTouchPosition;
            }
        }
    }
    public SnakeHeadActions @SnakeHead => new SnakeHeadActions(this);
    public interface ISnakeHeadActions
    {
        void OnMoving(InputAction.CallbackContext context);
        void OnTouchPosition(InputAction.CallbackContext context);
    }
}
