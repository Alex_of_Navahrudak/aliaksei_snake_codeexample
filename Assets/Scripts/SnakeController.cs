using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//The script defins the behavior of the Snake
public class SnakeController : MonoBehaviour
{
    public int accelerationLength = 5;
    public int diamondsCountForAcceleration = 4;
    public float movingSpeed = 20.0f;
    public float rotationSpeed = 20.0f;

    [SerializeField] private Funnel funnel;
    [SerializeField] private Text diamondsField;
    [SerializeField] private Text foodField;
    [SerializeField] private GameObject bodyPrefab;

    private HeadPosManager positioningManager;
    private Camera camera;
    private Renderer snakeRenderer;
    private Vector3 headScale = Vector3.one;
    private Vector3 tailScale = new Vector3(0.7f, 0.7f, 0.7f);

    private string diamondTag = "Diamond";
    private string foodTag = "Food";
    private string colorizatorTag = "Colorizator";
    private string borderTag = "Border";
    private string finishTag = "Finish";

    private int DiamondPoints { get; set; } = 0;
    private int FoodPoints { get; set; } = 0;
    private float RotationAngle { get; set; } = 0.0f;

    private int currentGap = 6;
    private readonly (int normal, int accelerated) gap = (6, 3);

    private List<GameObject> bodyParts = new List<GameObject>();
    private List<Vector3> headPosHistory = new List<Vector3>();
        
    private bool movingAllowed = false;
    private bool accelerationActive = false;

    void Awake()
    {
        positioningManager = HeadPosManager.Instance;
        camera = Camera.main;
    }

    private void Start()
    {
        snakeRenderer = gameObject.GetComponent<Renderer>();
        funnel.SetTargetColor(snakeRenderer.material.color);
        AddBodyPart();
    }

    private void OnEnable()
    {
        positioningManager.OnStartTouch += CountRotationAngle;
    }

    private void OnDisable()
    {
        positioningManager.OnEndTouch -= CountRotationAngle;
    }

    //Reads the cursor position using the Input System and counts the angle of head rotation
    public void CountRotationAngle(Vector2 screenPos, float time)
    {
        if (!movingAllowed || accelerationActive)
        {
            movingAllowed = true;
            return;
        }

        Vector3 sreenCoordinates = new Vector3(screenPos.x, screenPos.y, camera.nearClipPlane);
        Vector3 gameCoordinates = sreenCoordinates + new Vector3(-Screen.width / 2, 0);

        RotationAngle = 90 - Mathf.Atan2(gameCoordinates.y, gameCoordinates.x) * Mathf.Rad2Deg;
    }

    //Instanciates one more body part and colors in with the head color
    private void AddBodyPart()
    {
        GameObject bodyPart = Instantiate(bodyPrefab);
        bodyPart.GetComponent<Renderer>().material = snakeRenderer.material;
        bodyParts.Add(bodyPart);
    }

    //Colors the snake when it cross the Colored Border in the game
    private void SetSnakeMaterial(Material mat)
    {
        snakeRenderer.material = mat;

        foreach (var bodyPart in bodyParts)
            bodyPart.GetComponent<Renderer>().material = mat;
    }

    //Animates the process of food swalowing
    public IEnumerator Swallow()
    {
        FoodPoints++;
        foodField.text = FoodPoints.ToString();
        AddBodyPart();

        for (int i = 0; i < bodyParts.Count-1; i++)
        {
            bodyParts[i].transform.localScale = headScale;
            yield return new WaitForSeconds(0.1f);
            bodyParts[i].transform.localScale = tailScale;
        }
    }

    //Increases Snake speed for short time
    private IEnumerator Acceleration()
    {
        accelerationActive = true;
        currentGap = gap.accelerated;

        RotationAngle = 0;
        transform.localRotation = Quaternion.Euler(transform.rotation.x, 0, transform.rotation.z);
        camera.transform.position = new Vector3(0, camera.transform.position.y, camera.transform.position.z);

        movingSpeed *= 2;
        yield return new WaitForSeconds(accelerationLength);
        movingSpeed /= 2;

        currentGap = gap.normal;
        accelerationActive = false;
    }

    //Plus one diamond
    private void IncreaseDiamonds()
    {
        DiamondPoints++;
        diamondsField.text = DiamondPoints.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == foodTag)
        {
            Material foodMat = other.GetComponent<Renderer>().material;
            
            if (snakeRenderer.material.color == foodMat.color)
            {
                return;
            }
        }
        else if (other.tag == diamondTag)
        {
            other.gameObject.SetActive(false);
            IncreaseDiamonds();

            if (DiamondPoints % diamondsCountForAcceleration == 0 && !accelerationActive)
                StartCoroutine(Acceleration());

            return;
        }
        else if (other.tag == colorizatorTag)
        {
            Material newMaterial = other.GetComponent<Renderer>().material;
            SetSnakeMaterial(newMaterial);
            funnel.SetTargetColor(newMaterial.color);
            return;
        }
        else if (accelerationActive && other.tag != borderTag && other.tag != finishTag)
        {
            other.gameObject.SetActive(false);
            IncreaseDiamonds();
            return;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (!movingAllowed)
            return;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, RotationAngle, 0.0f), Time.deltaTime * rotationSpeed);

        camera.transform.position += transform.forward * movingSpeed * Time.deltaTime;
        headPosHistory.Insert(0, transform.position);

        for (int i = 0; i < bodyParts.Count; i++)
        {
            Vector3 point = headPosHistory[Mathf.Min(i * currentGap, headPosHistory.Count - 1)];
            Vector3 direction = point - bodyParts[i].transform.position;
            bodyParts[i].transform.position += direction * movingSpeed * Time.deltaTime;
            bodyParts[i].transform.LookAt(point);
        }
    }
}
