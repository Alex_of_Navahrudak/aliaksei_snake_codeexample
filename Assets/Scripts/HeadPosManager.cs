using UnityEngine;
using UnityEngine.InputSystem;

[DefaultExecutionOrder(1)]
public class HeadPosManager : SingletonComponent<HeadPosManager>
{
    public delegate void StartTouchEvent(Vector2 position, float time);
    public event StartTouchEvent OnStartTouch;
    public delegate void EndTouchEvent(Vector2 position, float time);
    public event EndTouchEvent OnEndTouch;

    private Controls controls;

    private void OnEnable() => controls.Enable();
    private void OnDisable() => controls.Disable();
    private void Awake() => controls = new Controls();

    private void Start()
    {
        controls.SnakeHead.Moving.started += ctx => StartTouch(ctx);
        controls.SnakeHead.Moving.canceled += ctx => EndTouch(ctx);
    }

    private void StartTouch(InputAction.CallbackContext context)
    {
        if (OnStartTouch != null)
            OnStartTouch(controls.SnakeHead.TouchPosition.ReadValue<Vector2>(), (float)context.startTime);
    }
    
    private void EndTouch(InputAction.CallbackContext context)
    {
        if (OnEndTouch != null)
            OnEndTouch(controls.SnakeHead.TouchPosition.ReadValue<Vector2>(), (float)context.startTime);
    }
}
